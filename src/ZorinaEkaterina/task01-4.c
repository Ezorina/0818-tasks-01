#include <stdio.h>
#include <string.h>

int main()
{
    char num[50];
    int i,j,sum,len;
	sum = 0;

    puts("Enter the number:\n");
    scanf("%s",&num);
	

    len = strlen(num);
    for (i=len; i>0; i-=3)
    {
        for (j=len; j>=i; j--)
            num[j+1]=num[j];
        num[i]=' ';
        len++;
    }

    puts("The number:\n");
    for (i=0; i<len; i++)
        putchar(num[i]);
    return 0;
}

#include <stdio.h>
#include <string.h>

int main()
{
	char str[50];
	int i, sum, num, len;
	puts("Enter the string");
	fgets(str,50,stdin);
	num = 0;
	sum = 0;
	len = strlen(str);

	for(i=0;i<len;i++)
	{
		if ((str[i]>='0') && (str[i]<='9'))
			num = num*10 + (int)str[i] - (int)'0';
		else
		{
			sum += num;
			num = 0;
		}
	}
	printf("Sum: %d\n",sum);
	return 0;
}